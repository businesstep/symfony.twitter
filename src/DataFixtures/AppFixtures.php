<?php

namespace App\DataFixtures;

use App\Entity\MicroPost;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private const USERS = [
        'username'  => 'ivan_popko',
        'email'     => 'ivan.popko@mail.ru',
        'password'  => 'ivan123',
        'fullName'  => 'Ivan Popko',
        'roles'     => [User::ROLE_ADMIN]
    ];
    /**
     * UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadMicroPosts($manager);
    }

    private function loadMicroPosts(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $microPost = new MicroPost();

            $microPost->setText('Some randow text '. rand(0, 100));
            $microPost->setUser($this->getReference(
                self::USERS[rand(0, count(self::USERS)-1)]['username']
            ));

            $date = new \DateTime();
            $date->modify('-' . rand(0, 10) . ' day');
            $microPost->setTime($date);

            $manager->persist($microPost);
        }

        $manager->flush();
    }

    private function loadUsers(ObjectManager $manager)
    {
        foreach(self::USERS as $data) {
            $user = new User();

            $user->setUsername($data['username']);
            $user->setFullName($data['fullName']);
            $user->setEmail($data['email']);
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user, $data['password'])
            );

            $user->setRoles($data['roles']);

            $this->addReference($data['username'], $user);

            $manager->persist($user);
        }

        $manager->flush();
    }
}
